import json

from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.routers import reverse
from rest_framework.test import APITestCase

from comment.models import Comment
from project.models import Project
from task.models import Task


class CommentTests(APITestCase):

    @classmethod
    def setUpTestData(cls):
        User.objects.create_user(
            username='admin',
            password='secret',
            is_staff=True,
        )
        cls.user = User.objects.create_user(
            username='user',
            password='secret'
        )
        cls.user2 = User.objects.create_user(
            username='user2',
            password='secret'
        )
        cls.project = Project.objects.create(name='project1')
        cls.tasks = Task.objects.bulk_create([
            Task(name='task1', author_id=cls.user.id, project_id=cls.project.id),
            Task(name='task2', author_id=cls.user.id, project_id=cls.project.id),
        ])
        cls.comments = Comment.objects.bulk_create([
            Comment(task_id=cls.tasks[0].id, text='comment1', author_id=cls.user2.id),
            Comment(task_id=cls.tasks[0].id, text='comment2', author_id=cls.user2.id),
            Comment(task_id=cls.tasks[0].id, text='comment3', author_id=cls.user2.id),
            Comment(task_id=cls.tasks[1].id, text='comment4', author_id=cls.user2.id),
        ])

    def test_comment_create_is_anonymous(self):
        url = reverse('comment-list')
        data = {'task_id': self.tasks[0].id,
                'text': 'comment test text'}
        response = self.client.post(url, data)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_comment_create(self):
        self.client.login(username='user', password='secret')

        url = reverse('comment-list')
        data = {'task': self.tasks[0].id,
                'text': 'comment test text'}
        response = self.client.post(url, data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(
            Comment.objects.filter(
                task_id=self.tasks[0].id, text='comment test text'
            ).exists())

    def test_comment_detail_is_anonymous(self):
        url = reverse('comment-detail', kwargs={'pk': self.comments[0].id})
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_comment_detail(self):
        self.client.login(username='user', password='secret')

        url = reverse('comment-detail', kwargs={'pk': self.comments[0].id})
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            json.loads(response.content.decode()),
            {
                'id': self.comments[0].id,
                'author': self.user2.id,
                'task': self.comments[0].task_id,
                'text': self.comments[0].text
            })

    def test_comment_list_is_anonymous(self):
        url = reverse('comment-list')
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_comment_list(self):
        self.client.login(username='user', password='secret')

        url = reverse('comment-list')
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 4)
        self.assertListEqual(
            json.loads(response.content.decode())['results'],
            [{
                'id': self.comments[0].id,
                'author': self.user2.id,
                'task': self.comments[0].task_id,
                'text': self.comments[0].text
            }, {
                'id': self.comments[1].id,
                'author': self.user2.id,
                'task': self.comments[1].task_id,
                'text': self.comments[1].text
            }, {
                'id': self.comments[2].id,
                'author': self.user2.id,
                'task': self.comments[2].task_id,
                'text': self.comments[2].text
            }, {
                'id': self.comments[3].id,
                'author': self.user2.id,
                'task': self.comments[3].task_id,
                'text': self.comments[3].text
            }]
        )

    def test_comment_update_is_anonymous(self):
        url = reverse('comment-detail', kwargs={'pk': self.comments[0].id})
        data = {'text': 'comment update text'}
        response = self.client.put(url, data)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_comment_update_is_not_owner(self):
        self.client.login(username='user', password='secret')

        url = reverse('comment-detail', kwargs={'pk': self.comments[0].id})
        data = {'text': 'comment update text'}
        response = self.client.put(url, data)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_comment_update_is_owner(self):
        self.client.login(username='user2', password='secret')

        url = reverse('comment-detail', kwargs={'pk': self.comments[0].id})
        data = {'text': 'comment update text'}
        response = self.client.put(url, data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        comment = Comment.objects.get(id=self.comments[0].id)
        self.assertEqual(comment.task_id, self.tasks[0].id)
        self.assertEqual(comment.text, 'comment update text')

        data = {'task': self.tasks[1].id,
                'text': 'comment2 update text'}
        response = self.client.put(url, data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        comment2 = Comment.objects.get(id=self.comments[0].id)
        self.assertEqual(comment2.task_id, self.tasks[0].id)
        self.assertEqual(comment2.text, 'comment2 update text')

    def test_comment_delete_is_anonymous(self):
        url = reverse('comment-detail', kwargs={'pk': self.comments[0].id})
        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_comment_delete_is_user(self):
        self.client.login(username='user', password='secret')

        url = reverse('comment-detail', kwargs={'pk': self.comments[0].id})
        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_comment_delete_is_admin(self):
        self.client.login(username='admin', password='secret')

        url = reverse('comment-detail', kwargs={'pk': self.comments[0].id})
        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Comment.objects.filter(id=self.comments[0].id).exists())
