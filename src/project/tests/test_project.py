import json

from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.routers import reverse
from rest_framework.test import APITestCase

from project.models import Project


class ProjectTests(APITestCase):

    @classmethod
    def setUpTestData(cls):
        User.objects.create_user(
            username='admin',
            password='secret',
            is_staff=True,
        )
        User.objects.create_user(
            username='user',
            password='secret'
        )
        cls.projects = Project.objects.bulk_create([
            Project(name='project1'),
            Project(name='project2'),
            Project(name='project3'),
        ])

    def test_project_create_is_anonymous(self):
        url = reverse('project-list')
        data = {'name': 'ProjectTest'}
        response = self.client.post(url, data)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_project_create(self):
        self.client.login(username='user', password='secret')

        url = reverse('project-list')
        data = {'name': 'ProjectTest'}
        response = self.client.post(url, data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(Project.objects.filter(name='ProjectTest').exists())

    def test_project_detail_is_anonymous(self):
        url = reverse('project-detail', kwargs={'pk': self.projects[0].id})
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_project_detail(self):
        self.client.login(username='user', password='secret')

        url = reverse('project-detail', kwargs={'pk': self.projects[0].id})
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            json.loads(response.content.decode()),
            {
                'id': self.projects[0].id,
                'name': self.projects[0].name
            })

    def test_project_list_is_anonymous(self):
        url = reverse('project-list')
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_project_list(self):
        self.client.login(username='user', password='secret')

        url = reverse('project-list')
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 3)
        self.assertListEqual(
            json.loads(response.content.decode())['results'],
            [{
                'id': self.projects[0].id,
                'name': self.projects[0].name
            }, {
                'id': self.projects[1].id,
                'name': self.projects[1].name
            }, {
                'id': self.projects[2].id,
                'name': self.projects[2].name
            }]
        )

    def test_project_update_is_anonymous(self):
        url = reverse('project-detail', kwargs={'pk': self.projects[0].id})
        data = {'name': 'ProjectTest'}
        response = self.client.put(url, data)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_project_update(self):
        self.client.login(username='user', password='secret')

        url = reverse('project-detail', kwargs={'pk': self.projects[0].id})
        data = {'name': 'UpdateProject'}
        response = self.client.put(url, data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Project.objects.get(id=self.projects[0].id).name, 'UpdateProject')

    def test_project_delete_is_anonymous(self):
        url = reverse('project-detail', kwargs={'pk': self.projects[0].id})
        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_project_delete_is_user(self):
        self.client.login(username='user', password='secret')

        url = reverse('project-detail', kwargs={'pk': self.projects[0].id})
        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_comment_delete_is_admin(self):
        self.client.login(username='admin', password='secret')

        url = reverse('project-detail', kwargs={'pk': self.projects[0].id})
        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Project.objects.filter(id=self.projects[0].id).exists())
