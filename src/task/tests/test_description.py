import json

from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.routers import reverse
from rest_framework.test import APITestCase

from project.models import Project
from task.models import Task, Description


class DescriptionTests(APITestCase):

    @classmethod
    def setUpTestData(cls):
        User.objects.create_user(
            username='admin',
            password='secret',
            is_staff=True,
        )
        cls.user = User.objects.create_user(
            username='user',
            password='secret'
        )
        cls.project = Project.objects.create(name='project1')
        cls.tasks = Task.objects.bulk_create([
            Task(name='task1', author_id=cls.user.id, project_id=cls.project.id),
            Task(name='task2', author_id=cls.user.id, project_id=cls.project.id),
        ])
        cls.descriptions = Description.objects.bulk_create([
            Description(task_id=cls.tasks[0].id, text='description1'),
            Description(task_id=cls.tasks[0].id, text='description2'),
            Description(task_id=cls.tasks[0].id, text='description3'),
            Description(task_id=cls.tasks[1].id, text='description4'),
        ])

    def test_description_create_is_anonymous(self):
        url = reverse('description-list')
        data = {'task': self.tasks[0].id,
                'text': 'description test text'}
        response = self.client.post(url, data)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_description_create(self):
        self.client.login(username='user', password='secret')

        url = reverse('description-list')
        data = {'task': self.tasks[0].id,
                'text': 'description test text'}
        response = self.client.post(url, data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(
            Description.objects.filter(
                task_id=self.tasks[0].id,
                text='description test text'
            ).exists())

    def test_description_detail_is_anonymous(self):
        url = reverse('description-detail', kwargs={'pk': self.descriptions[0].id})
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_description_detail(self):
        self.client.login(username='user', password='secret')

        url = reverse('description-detail', kwargs={'pk': self.descriptions[0].id})
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            json.loads(response.content.decode()),
            {
                'id': self.descriptions[0].id,
                'task': self.descriptions[0].task_id,
                'text': self.descriptions[0].text
            })

    def test_description_list_is_anonymous(self):
        url = reverse('description-list')
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_description_list(self):
        self.client.login(username='user', password='secret')

        url = reverse('description-list')
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 4)
        self.assertListEqual(
            json.loads(response.content.decode())['results'],
            [{
                'id': self.descriptions[0].id,
                'task': self.descriptions[0].task_id,
                'text': self.descriptions[0].text
            }, {
                'id': self.descriptions[1].id,
                'task': self.descriptions[1].task_id,
                'text': self.descriptions[1].text
            }, {
                'id': self.descriptions[2].id,
                'task': self.descriptions[2].task_id,
                'text': self.descriptions[2].text
            }, {
                'id': self.descriptions[3].id,
                'task': self.descriptions[3].task_id,
                'text': self.descriptions[3].text
            }]
        )

    def test_description_update_is_anonymous(self):
        url = reverse('description-detail', kwargs={'pk': self.descriptions[0].id})
        data = {'text': 'description update text'}
        response = self.client.put(url, data)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_description_update(self):
        self.client.login(username='user', password='secret')

        url = reverse('description-detail', kwargs={'pk': self.descriptions[0].id})
        data = {'task': self.tasks[1].id,
                'text': 'description update text'}
        response = self.client.put(url, data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        description = Description.objects.get(id=self.descriptions[0].id)
        self.assertEqual(description.task_id, self.tasks[0].id)
        self.assertEqual(description.text, 'description update text')

    def test_description_delete_is_anonymous(self):
        url = reverse('description-detail', kwargs={'pk': self.descriptions[0].id})
        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_description_delete_is_user(self):
        self.client.login(username='user', password='secret')

        url = reverse('description-detail', kwargs={'pk': self.descriptions[0].id})
        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_description_delete_is_admin(self):
        self.client.login(username='admin', password='secret')

        url = reverse('description-detail', kwargs={'pk': self.descriptions[0].id})
        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Description.objects.filter(id=self.descriptions[0].id).exists())
