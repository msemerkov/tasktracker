import json

from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.routers import reverse
from rest_framework.test import APITestCase

from comment.models import Comment
from project.models import Project
from task.models import Task, Description


class ProjectTests(APITestCase):

    @classmethod
    def setUpTestData(cls):
        User.objects.create_user(
            username='admin',
            password='secret',
            is_staff=True,
        )
        cls.user = User.objects.create_user(
            username='user',
            password='secret'
        )
        cls.user2 = User.objects.create_user(
            username='user2',
            password='secret'
        )
        cls.projects = Project.objects.bulk_create([
            Project(name='project1'),
            Project(name='project2'),
            Project(name='project3'),
        ])
        cls.tasks = Task.objects.bulk_create([
            Task(project_id=cls.projects[0].id, name='task1', author=cls.user),
            Task(project_id=cls.projects[0].id, name='task2', author=cls.user2),
            Task(project_id=cls.projects[1].id, name='task3', author=cls.user2),
            Task(project_id=cls.projects[2].id, name='task4', author=cls.user),
        ])
        cls.descriptions = Description.objects.bulk_create([
            Description(task_id=cls.tasks[0].id, text='description1'),
            Description(task_id=cls.tasks[0].id, text='description2'),
            Description(task_id=cls.tasks[0].id, text='description3'),
            Description(task_id=cls.tasks[1].id, text='description4'),
        ])
        cls.comments = Comment.objects.bulk_create([
            Comment(task_id=cls.tasks[3].id, text='comment1', author_id=cls.user.id),
            Comment(task_id=cls.tasks[3].id, text='comment2', author_id=cls.user2.id),
            Comment(task_id=cls.tasks[0].id, text='comment3', author_id=cls.user.id),
        ])

    def test_task_create_is_anonymous(self):
        url = reverse('task-list')
        data = {'project': self.projects[0].id,
                'name': 'TaskTest'}
        response = self.client.post(url, data)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_task_create(self):
        self.client.login(username='user', password='secret')

        url = reverse('task-list')
        data = {'project': self.projects[0].id,
                'name': 'TaskTest'}
        response = self.client.post(url, data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        task = Task.objects.filter(project_id=self.projects[0].id)
        self.assertTrue(task.exists())
        self.assertEqual(task[0].name, 'TaskTest')
        self.assertEqual(task[0].author, self.user)

    def test_task_detail_is_anonymous(self):
        url = reverse('task-detail', kwargs={'pk': self.tasks[0].id})
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_task_detail(self):
        self.client.login(username='user', password='secret')

        url = reverse('task-detail', kwargs={'pk': self.tasks[0].id})
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            json.loads(response.content.decode()),
            {
                'id': self.tasks[0].id,
                'project': self.projects[0].id,
                'name': 'task1',
                'status': 'open',
                'performer': None,
                'author': self.user.id,
                'comment': [{
                    'id': self.comments[2].id,
                    'author': self.user.id,
                    'task': self.comments[2].task_id,
                    'text': 'comment3'
                }],
                'description': [{
                    "id": self.descriptions[0].id,
                    "task": self.tasks[0].id,
                    "text": "description1"
                }, {
                    "id": self.descriptions[1].id,
                    "task": self.tasks[0].id,
                    "text": "description2"
                }, {
                    "id": self.descriptions[2].id,
                    "task": self.tasks[0].id,
                    "text": "description3"
                }]
            })

    def test_task_list_is_anonymous(self):
        url = reverse('task-list')
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_task_list(self):
        self.client.login(username='user', password='secret')

        url = reverse('task-list')
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 4)
        self.assertListEqual(
            json.loads(response.content.decode())['results'],
            [{
                'id': self.tasks[3].id,
                'project': self.projects[2].id,
                'name': 'task4',
                'status': 'open',
                'performer': None,
                'author': self.user.id,
                'comment': [{
                    'id': self.comments[0].id,
                    'author': self.user.id,
                    'task': self.comments[0].task_id,
                    'text': 'comment1'
                },{
                    'id': self.comments[1].id,
                    'author': self.user2.id,
                    'task': self.comments[1].task_id,
                    'text': 'comment2'
                }],
                'description': []
            }, {
                'id': self.tasks[2].id,
                'project': self.projects[1].id,
                'name': 'task3',
                'status': 'open',
                'performer': None,
                'author': self.user2.id,
                'comment': [],
                'description': []
            }, {
                'id': self.tasks[1].id,
                'project': self.projects[0].id,
                'name': 'task2',
                'status': 'open',
                'performer': None,
                'author': self.user2.id,
                'comment': [],
                'description': [{
                    "id": self.descriptions[3].id,
                    "task": self.tasks[1].id,
                    "text": "description4"
                }]
            }, {
                'id': self.tasks[0].id,
                'project': self.projects[0].id,
                'name': 'task1',
                'status': 'open',
                'performer': None,
                'author': self.user.id,
                'comment': [{
                    'id': self.comments[2].id,
                    'author': self.user.id,
                    'task': self.comments[2].task_id,
                    'text': 'comment3'
                }],
                'description': [{
                    "id": self.descriptions[0].id,
                    "task": self.tasks[0].id,
                    "text": "description1"
                }, {
                    "id": self.descriptions[1].id,
                    "task": self.tasks[0].id,
                    "text": "description2"
                }, {
                    "id": self.descriptions[2].id,
                    "task": self.tasks[0].id,
                    "text": "description3"
                }]
            }]
        )

    def test_task_update_is_anonymous(self):
        url = reverse('task-detail', kwargs={'pk': self.tasks[1].id})
        data = {'name': 'UpdateTest',
                'status': 'resolve',
                'performer': self.user.id}
        response = self.client.put(url, data)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_task_update_is_not_owner(self):
        url = reverse('task-detail', kwargs={'pk': self.tasks[1].id})
        data = {'name': 'UpdateTest',
                'status': 'resolve',
                'performer': self.user.id}
        response = self.client.put(url, data)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_task_update(self):
        self.client.login(username='user', password='secret')

        url = reverse('task-detail', kwargs={'pk': self.tasks[1].id})
        data = {'project': self.projects[1].id,
                'name': 'taskUpdate',
                'status': 'resolve',
                'performer': self.user.id}
        response = self.client.put(url, data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        task = Task.objects.get(id=self.tasks[1].id)
        self.assertEqual(task.project_id, self.projects[1].id)
        self.assertEqual(task.name, 'taskUpdate')
        self.assertEqual(task.status, 'resolve')
        self.assertEqual(task.performer_id, self.user.id)

    def test_task_delete_is_anonymous(self):
        url = reverse('task-detail', kwargs={'pk': self.tasks[0].id})
        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_task_delete_is_user(self):
        self.client.login(username='user', password='secret')

        url = reverse('task-detail', kwargs={'pk': self.tasks[0].id})
        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_task_delete_is_admin(self):
        self.client.login(username='admin', password='secret')

        url = reverse('task-detail', kwargs={'pk': self.tasks[0].id})
        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Task.objects.filter(id=self.tasks[0].id).exists())
